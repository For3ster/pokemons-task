package circula.pokemons.common_mvi

import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment

fun Fragment.setOnBackPressedCallback(callback: () -> Unit) {
    activity?.onBackPressedDispatcher?.addCallback(
        viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                callback()
            }
        }
    )
}
