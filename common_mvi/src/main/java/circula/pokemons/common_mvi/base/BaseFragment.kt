package circula.pokemons.common_mvi.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.google.android.material.snackbar.Snackbar
import vivid.money.elmslie.android.base.ElmFragment
import vivid.money.elmslie.android.screen.ElmDelegate

abstract class BaseFragment<Event : Any, Effect : Any, State : Any>(
    @LayoutRes val layoutResId: Int,
) : ElmFragment<Event, Effect, State>(layoutResId),
    ElmDelegate<Event, Effect, State> {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(layoutResId, container, false)

    protected fun showError(error: String) {
        view?.let { Snackbar.make(it, error, Snackbar.LENGTH_SHORT).show() }
    }
}
