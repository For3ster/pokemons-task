package circula.pokemons.domain_pokemons.feature_api

import com.github.terrakok.cicerone.Screen

interface PokemonsFeatureApi {

    fun getScreen(): Screen
}
