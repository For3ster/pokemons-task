package circula.pokemons.domain_pokemons

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import circula.pokemons.domain_pokemons.dto.PokemonEntity

@Dao
interface PokemonsDao {

    @Query("SELECT * FROM pokemons")
    fun getPokemons(): List<PokemonEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(pokemons: List<PokemonEntity>)
}
