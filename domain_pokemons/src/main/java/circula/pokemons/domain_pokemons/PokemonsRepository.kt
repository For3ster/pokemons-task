package circula.pokemons.domain_pokemons

import circula.pokemons.domain_pokemons.dto.Pokemon
import circula.pokemons.domain_pokemons.dto.PokemonEntity
import javax.inject.Inject

private const val UPDATE_TIME = 300000L

class PokemonsRepository @Inject constructor(
    private val dao: PokemonsDao,
    private val api: PokemonApi,
) {

    suspend fun getPokemons(): List<Pokemon> {
        val localPokemons = dao.getPokemons()
        return if (localPokemons.isNotEmpty()) {
            localPokemons.toDto()
        } else {
            updatePokemons()
        }
    }

    suspend fun updatePokemons(): List<Pokemon> {
        val localPokemons = dao.getPokemons()
        return if (localPokemons.isNotEmpty() &&
            localPokemons.first().updatedAt >= System.currentTimeMillis() - UPDATE_TIME
        ) {
            localPokemons.toDto()
        } else {
            val remotePokemons = api.getPokemons().pokemons
            dao.insertAll(remotePokemons.toEntity())
            remotePokemons
        }
    }
}

fun List<PokemonEntity>.toDto(): List<Pokemon> =
    map {
        Pokemon(
            id = it.id,
            name = it.name,
            imagePath = it.imagePath
        )
    }

fun List<Pokemon>.toEntity(): List<PokemonEntity> =
    map {
        PokemonEntity(
            id = it.id,
            name = it.name,
            imagePath = it.imagePath,
            updatedAt = System.currentTimeMillis()
        )
    }
