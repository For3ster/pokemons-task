package circula.pokemons.domain_pokemons

import circula.pokemons.domain_pokemons.dto.Pokemons
import retrofit2.http.GET

interface PokemonApi {

    @GET("pokemon")
    suspend fun getPokemons(): Pokemons
}
