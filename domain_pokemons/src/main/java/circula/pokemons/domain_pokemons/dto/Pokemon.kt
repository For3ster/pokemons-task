package circula.pokemons.domain_pokemons.dto

data class Pokemon(
    val id: String,
    val name: String,
    val imagePath: String,
)

@JvmInline
value class Pokemons(val pokemons: List<Pokemon>)
