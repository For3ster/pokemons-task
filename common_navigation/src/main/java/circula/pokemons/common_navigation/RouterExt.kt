package circula.pokemons.common_navigation

import androidx.fragment.app.Fragment
import circula.pokemons.common_navigation.di.RouterHolder
import com.github.terrakok.cicerone.Router

inline val Fragment.router: Router
    get() = (requireContext().applicationContext as RouterHolder).router
