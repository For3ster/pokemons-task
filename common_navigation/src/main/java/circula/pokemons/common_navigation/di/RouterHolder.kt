package circula.pokemons.common_navigation.di

import com.github.terrakok.cicerone.Router

interface RouterHolder {
    val router: Router
}
