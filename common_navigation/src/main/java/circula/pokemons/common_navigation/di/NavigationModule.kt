package circula.pokemons.common_navigation.di

import com.github.terrakok.cicerone.Cicerone
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object NavigationModule {

    private val cicerone = Cicerone.create()

    @Provides
    @Singleton
    fun provideMainRouter() = cicerone.router

    @Provides
    @Singleton
    fun provideMainNavHolder() = cicerone.getNavigatorHolder()
}
