package circula.pokemons.di

import android.app.Application
import android.content.Context
import circula.pokemons.common_database.di.DatabaseModule
import circula.pokemons.common_navigation.di.NavigationModule
import circula.pokemons.common_network.di.NetworkModule
import circula.pokemons.feature_photo.di.PhotoModule
import circula.pokemons.feature_pokemons.di.PokemonsDependencies
import circula.pokemons.feature_pokemons.di.PokemonsModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        NavigationModule::class,
        NetworkModule::class,
        PokemonsModule::class,
        PhotoModule::class,
        DatabaseModule::class,
    ]
)
interface AppComponent :
    MainDependencies,
    PokemonsDependencies {

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance context: Context,
            @BindsInstance application: Application
        ): AppComponent
    }
}
