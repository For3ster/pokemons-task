package circula.pokemons.di

import circula.pokemons.domain_pokemons.feature_api.PokemonsFeatureApi
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.Router

interface MainDependencies {

    val router: Router
    val navHolder: NavigatorHolder
    val pokemonsFeatureApi: PokemonsFeatureApi
}
