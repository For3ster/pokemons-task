package circula.pokemons

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import circula.pokemons.common_di.getComponent
import circula.pokemons.di.MainDependencies
import com.github.terrakok.cicerone.androidx.AppNavigator

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val component by lazy { getComponent<MainDependencies>() }
    private val navHolder by lazy { component.navHolder }
    private val navigator by lazy { AppNavigator(this, R.id.container) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.router.newRootScreen(component.pokemonsFeatureApi.getScreen())
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navHolder.removeNavigator()
        super.onPause()
    }
}
