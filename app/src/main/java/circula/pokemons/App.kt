package circula.pokemons

import android.app.Application
import circula.pokemons.common_di.ComponentHolder
import circula.pokemons.common_navigation.di.RouterHolder
import circula.pokemons.di.AppComponent
import circula.pokemons.di.DaggerAppComponent
import com.github.terrakok.cicerone.Router

class App : Application(),
    ComponentHolder<Any>,
    RouterHolder {

    lateinit var appComponent: AppComponent

    override val router: Router
        get() = appComponent.router

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.factory().create(this, this)
    }

    override fun getComponent(componentClass: Class<out Any>): Any =
        if (componentClass.isInstance(appComponent)) {
            appComponent
        } else {
            error("Component $componentClass is not declared")
        }
}
