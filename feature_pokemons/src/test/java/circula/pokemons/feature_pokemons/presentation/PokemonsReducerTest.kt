package circula.pokemons.feature_pokemons.presentation

import circula.pokemons.domain_pokemons.dto.Pokemon
import circula.pokemons.feature_pokemons.presentation.PokemonsCommand.GetPokemons
import circula.pokemons.feature_pokemons.presentation.PokemonsCommand.UpdatePokemons
import circula.pokemons.feature_pokemons.presentation.PokemonsEffect.OpenPhoto
import circula.pokemons.feature_pokemons.presentation.PokemonsEffect.ShowError
import circula.pokemons.feature_pokemons.presentation.PokemonsEvent.Internal
import circula.pokemons.feature_pokemons.presentation.PokemonsEvent.Ui
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import org.mockito.kotlin.mock

internal class PokemonsReducerTest : BehaviorSpec({
    val reducer = PokemonsReducer

    Given("Initial State") {
        When("Ui.System.Init") {
            val (state, effects, commands) = reducer.reduce(Ui.System.Init, STATE)
            Then("check state") { state shouldBe STATE }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") { commands.shouldContainExactly(GetPokemons) }
        }

        When("Ui.System.Stop") {
            val (state, effects, commands) = reducer.reduce(Ui.System.Stop, STATE)
            Then("check state") { state shouldBe STATE }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") { commands.shouldContainExactly(UpdatePokemons) }
        }

        When("Internal.GetPokemonsSuccess") {
            val (state, effects, commands) =
                reducer.reduce(Internal.GetPokemonsSuccess(POKEMONS), STATE)

            Then("check state") {
                state.pokemons shouldBe POKEMONS
                state.isLoading.shouldBeFalse()
            }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") { commands.shouldBeEmpty() }
        }

        When("Internal.GetPokemonsError") {
            val (state, effects, commands) =
                reducer.reduce(Internal.GetPokemonsError(ERROR), STATE)

            Then("check state") {
                state.pokemons.shouldBeEmpty()
                state.isLoading.shouldBeFalse()
            }
            Then("check effects") { effects.shouldContainExactly(ShowError(ERROR)) }
            Then("check commands") { commands.shouldBeEmpty() }
        }

        When("Internal.UpdatePokemonsSuccess") {
            val (state, effects, commands) =
                reducer.reduce(Internal.UpdatePokemonsSuccess(POKEMONS), STATE)

            Then("check state") { state.pokemons shouldBe POKEMONS }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") { commands.shouldBeEmpty() }
        }

        When("Internal.UpdatePokemonsError") {
            val (state, effects, commands) =
                reducer.reduce(Internal.UpdatePokemonsError(ERROR), STATE)

            Then("check state") { state shouldBe STATE }
            Then("check effects") { effects.shouldContainExactly(ShowError(ERROR)) }
            Then("check commands") { commands.shouldBeEmpty() }
        }
    }

    Given("Loaded pokemons") {
        val givenState = STATE.copy(pokemons = POKEMONS, isLoading = false)

        When("Ui.Click.PokemonItem") {
            val (state, effects, commands) =
                reducer.reduce(Ui.Click.PokemonItem(POKEMON), givenState)

            Then("check state") { state shouldBe givenState }
            Then("check effects") {
                effects.shouldContainExactly(OpenPhoto(POKEMON))
            }
            Then("check commands") { commands.shouldBeEmpty() }
        }
    }
}) {

    private companion object {

        val STATE = PokemonsState()
        val POKEMON: Pokemon = mock()
        val POKEMONS = List(5) { POKEMON }
        val ERROR = Throwable()
    }
}
