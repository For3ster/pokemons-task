package circula.pokemons.feature_pokemons.list

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import circula.pokemons.feature_pokemons.databinding.ItemPokemonBinding
import coil.load

internal class PokemonItemViewHolder(
    itemView: View,
    private val onItemClick: (PokemonItem) -> Unit,
) : RecyclerView.ViewHolder(itemView) {

    private val binding = ItemPokemonBinding.bind(itemView)

    fun bind(item: PokemonItem) = with(binding) {
        image.load(item.imageUrl)
        name.text = item.name
        root.setOnClickListener { onItemClick(item) }
    }
}
