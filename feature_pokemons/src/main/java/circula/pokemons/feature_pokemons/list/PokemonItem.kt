package circula.pokemons.feature_pokemons.list

import circula.pokemons.domain_pokemons.dto.Pokemon

data class PokemonItem(
    val id: String,
    val name: String,
    val imageUrl: String,
    val data: Pokemon,
)
