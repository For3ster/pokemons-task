package circula.pokemons.feature_pokemons.list

import circula.pokemons.feature_pokemons.presentation.PokemonsState

internal object PokemonListConverter {

    fun map(state: PokemonsState): List<PokemonItem> =
        state.pokemons.map { pokemon ->
            PokemonItem(
                id = pokemon.id,
                name = pokemon.name,
                imageUrl = pokemon.imagePath,
                data = pokemon
            )
        }
}
