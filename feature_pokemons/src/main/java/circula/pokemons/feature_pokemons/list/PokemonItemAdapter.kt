package circula.pokemons.feature_pokemons.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import circula.pokemons.feature_pokemons.databinding.ItemPokemonBinding

internal class PokemonItemAdapter(
    private val onItemClick: (PokemonItem) -> Unit,
) : ListAdapter<PokemonItem, PokemonItemViewHolder>(ItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemPokemonBinding.inflate(inflater, parent, false)

        return PokemonItemViewHolder(binding.root, onItemClick)
    }

    override fun onBindViewHolder(holder: PokemonItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

private class ItemDiffCallback : DiffUtil.ItemCallback<PokemonItem>() {

    override fun areItemsTheSame(oldItem: PokemonItem, newItem: PokemonItem): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: PokemonItem, newItem: PokemonItem): Boolean =
        oldItem.name == newItem.name
}
