package circula.pokemons.feature_pokemons.di

import circula.pokemons.domain_pokemons.PokemonApi
import circula.pokemons.domain_pokemons.feature_api.PokemonsFeatureApi
import circula.pokemons.feature_pokemons.feature_api.PokemonsFeatureApiImpl
import circula.pokemons.feature_pokemons.presentation.PokemonsStoreFactory
import circula.pokemons.feature_pokemons.presentation.PokemonsStoreFactoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

private const val BASE_URL = "https://interview.circula.com/"

@Module(includes = [PokemonsBindingsModule::class])
object PokemonsModule {

    @Provides
    @Singleton
    fun providePokemonApi(
        builder: Retrofit.Builder,
    ): PokemonApi =
        builder
            .baseUrl(BASE_URL)
            .build()
            .create(PokemonApi::class.java)
}

@Module
abstract class PokemonsBindingsModule {

    @Binds
    internal abstract fun bindPokemonsFeatureApi(impl: PokemonsFeatureApiImpl): PokemonsFeatureApi

    @Binds
    internal abstract fun bindPokemonsStoreFactory(impl: PokemonsStoreFactoryImpl): PokemonsStoreFactory
}
