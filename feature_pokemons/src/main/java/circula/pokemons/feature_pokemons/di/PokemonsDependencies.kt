package circula.pokemons.feature_pokemons.di

import circula.pokemons.domain_photo.feature_api.PhotoFeatureApi
import circula.pokemons.feature_pokemons.presentation.PokemonsStoreFactory

interface PokemonsDependencies {

    val pokemonsStoreFactory: PokemonsStoreFactory
    val photoFeatureApi: PhotoFeatureApi
}
