package circula.pokemons.feature_pokemons.feature_api

import circula.pokemons.domain_pokemons.feature_api.PokemonsFeatureApi
import circula.pokemons.feature_pokemons.PokemonsFragment
import com.github.terrakok.cicerone.Screen
import com.github.terrakok.cicerone.androidx.FragmentScreen
import javax.inject.Inject

internal class PokemonsFeatureApiImpl @Inject constructor() : PokemonsFeatureApi {

    override fun getScreen(): Screen =
        FragmentScreen { PokemonsFragment.newInstance() }
}
