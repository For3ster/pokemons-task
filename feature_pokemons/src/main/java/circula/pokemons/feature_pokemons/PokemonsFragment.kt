package circula.pokemons.feature_pokemons

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import circula.pokemons.common_di.getComponent
import circula.pokemons.common_mvi.base.BaseFragment
import circula.pokemons.common_mvi.setOnBackPressedCallback
import circula.pokemons.common_navigation.router
import circula.pokemons.feature_pokemons.databinding.FragmentPokemonsBinding
import circula.pokemons.feature_pokemons.di.PokemonsDependencies
import circula.pokemons.feature_pokemons.list.PokemonItem
import circula.pokemons.feature_pokemons.list.PokemonItemAdapter
import circula.pokemons.feature_pokemons.list.PokemonListConverter
import circula.pokemons.feature_pokemons.presentation.PokemonsEffect
import circula.pokemons.feature_pokemons.presentation.PokemonsEvent.Ui
import circula.pokemons.feature_pokemons.presentation.PokemonsState
import vivid.money.elmslie.android.storeholder.LifecycleAwareStoreHolder
import vivid.money.elmslie.android.storeholder.StoreHolder
import circula.pokemons.feature_pokemons.presentation.PokemonsEffect as Effect
import circula.pokemons.feature_pokemons.presentation.PokemonsEvent as Event
import circula.pokemons.feature_pokemons.presentation.PokemonsState as State

internal class PokemonsFragment : BaseFragment<Event, Effect, State>(R.layout.fragment_pokemons) {

    private val component by lazy { getComponent<PokemonsDependencies>() }

    override val initEvent: Event = Ui.System.Init

    override val storeHolder: StoreHolder<Event, Effect, State> =
        LifecycleAwareStoreHolder(
            lifecycle = lifecycle,
            storeProvider = { component.pokemonsStoreFactory.create() }
        )

    private val binding by viewBinding(FragmentPokemonsBinding::bind)
    private lateinit var adapter: PokemonItemAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.adapter = createAdapter().also { adapter = it }
        setOnBackPressedCallback { router.exit() }
    }

    override fun onStop() {
        store.accept(Ui.System.Stop)
        super.onStop()
    }

    override fun render(state: PokemonsState) {
        binding.progressView.root.isVisible = state.isLoading
    }

    override fun mapList(state: PokemonsState): List<PokemonItem> =
        PokemonListConverter.map(state)

    @Suppress("UNCHECKED_CAST")
    override fun renderList(state: PokemonsState, list: List<Any>) {
        adapter.submitList(list as List<PokemonItem>)
    }

    override fun handleEffect(effect: PokemonsEffect) = when (effect) {
        is Effect.OpenPhoto -> {
            val screen = component.photoFeatureApi.getScreen(
                title = effect.pokemon.name,
                imageUrl = effect.pokemon.imagePath
            )
            router.navigateTo(screen)
        }

        is Effect.ShowError -> showError(effect.error.toString())
    }

    private fun createAdapter() = PokemonItemAdapter { item ->
        store.accept(Ui.Click.PokemonItem(item.data))
    }

    companion object {

        fun newInstance(): Fragment = PokemonsFragment()
    }
}
