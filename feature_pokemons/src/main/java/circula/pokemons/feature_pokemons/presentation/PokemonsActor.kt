package circula.pokemons.feature_pokemons.presentation

import circula.pokemons.common_mvi.actorFlow
import circula.pokemons.domain_pokemons.PokemonsRepository
import circula.pokemons.feature_pokemons.presentation.PokemonsCommand.GetPokemons
import circula.pokemons.feature_pokemons.presentation.PokemonsCommand.UpdatePokemons
import circula.pokemons.feature_pokemons.presentation.PokemonsEvent.Internal
import kotlinx.coroutines.flow.Flow
import vivid.money.elmslie.coroutines.Actor
import javax.inject.Inject

internal class PokemonsActor @Inject constructor(
    private val repository: PokemonsRepository,
) : Actor<PokemonsCommand, Internal> {

    override fun execute(command: PokemonsCommand): Flow<Internal> =
        when (command) {
            is GetPokemons -> actorFlow {
                repository.getPokemons()
            }.mapEvents(Internal::GetPokemonsSuccess, Internal::GetPokemonsError)

            is UpdatePokemons -> actorFlow {
                repository.updatePokemons()
            }.mapEvents(Internal::UpdatePokemonsSuccess, Internal::UpdatePokemonsError)
        }
}
