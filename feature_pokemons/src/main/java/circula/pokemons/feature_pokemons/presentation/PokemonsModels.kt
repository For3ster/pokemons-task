package circula.pokemons.feature_pokemons.presentation

import circula.pokemons.domain_pokemons.dto.Pokemon
import vivid.money.elmslie.core.store.Store

typealias PokemonsStore = Store<PokemonsEvent, PokemonsEffect, PokemonsState>

data class PokemonsState(
    val pokemons: List<Pokemon> = emptyList(),
    val isLoading: Boolean = true,
)

sealed interface PokemonsEvent {

    sealed interface Ui : PokemonsEvent {
        object System {
            object Init : Ui
            object Stop : Ui
        }

        object Click {
            data class PokemonItem(val pokemon: Pokemon) : Ui
        }
    }

    sealed interface Internal : PokemonsEvent {
        data class GetPokemonsSuccess(val pokemons: List<Pokemon>) : Internal
        data class GetPokemonsError(val error: Throwable) : Internal

        data class UpdatePokemonsSuccess(val pokemons: List<Pokemon>) : Internal
        data class UpdatePokemonsError(val error: Throwable) : Internal
    }
}

sealed interface PokemonsEffect {
    data class ShowError(val error: Throwable) : PokemonsEffect
    data class OpenPhoto(val pokemon: Pokemon) : PokemonsEffect
}

sealed interface PokemonsCommand {
    object GetPokemons : PokemonsCommand
    object UpdatePokemons : PokemonsCommand
}
