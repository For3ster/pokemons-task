package circula.pokemons.feature_pokemons.presentation

import vivid.money.elmslie.coroutines.ElmStoreCompat
import javax.inject.Inject

interface PokemonsStoreFactory {

    fun create(): PokemonsStore
}

internal class PokemonsStoreFactoryImpl @Inject constructor(
    private val actor: PokemonsActor,
) : PokemonsStoreFactory {

    override fun create(): PokemonsStore = ElmStoreCompat(
        initialState = PokemonsState(),
        reducer = PokemonsReducer,
        actor = actor
    )
}
