package circula.pokemons.feature_pokemons.presentation

import circula.pokemons.feature_pokemons.presentation.PokemonsEvent.Internal
import circula.pokemons.feature_pokemons.presentation.PokemonsEvent.Ui
import vivid.money.elmslie.core.store.dsl_reducer.ScreenDslReducer
import circula.pokemons.feature_pokemons.presentation.PokemonsCommand as Command
import circula.pokemons.feature_pokemons.presentation.PokemonsEffect as Effect
import circula.pokemons.feature_pokemons.presentation.PokemonsEvent as Event
import circula.pokemons.feature_pokemons.presentation.PokemonsState as State

internal object PokemonsReducer : ScreenDslReducer<Event, Ui, Internal,
        State, Effect, Command>(Ui::class, Internal::class) {

    override fun Result.ui(event: Ui) = when (event) {
        is Ui.System.Init -> commands { +Command.GetPokemons }
        is Ui.System.Stop -> commands { +Command.UpdatePokemons }
        is Ui.Click.PokemonItem -> effects { +Effect.OpenPhoto(event.pokemon) }
    }

    override fun Result.internal(event: Internal) = when (event) {
        is Internal.GetPokemonsSuccess -> state {
            copy(pokemons = event.pokemons, isLoading = false)
        }

        is Internal.GetPokemonsError -> {
            state { copy(isLoading = false) }
            effects { +Effect.ShowError(event.error) }
        }

        is Internal.UpdatePokemonsSuccess -> {
            state { copy(pokemons = event.pokemons) }
        }

        is Internal.UpdatePokemonsError -> {
            effects { +Effect.ShowError(event.error) }
        }
    }
}
