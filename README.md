# Circula | Pokemons Task

The decisions made in this project were intended to provide an example of the implementation of a potentially scalable project.
For smaller applications, simpler solutions can be used, such as abandoning multimodularity, using Hilt instead of Dagger, etc.

To build the architecture inside the module, Elmslie library was chosen, which is a minimalistic reactive implementation of TEA/ELM.
This is one of many potential options, but it was interesting to show a variation of MVI that has a fairly low entry threshold and is simply supported by developers on a potentially large and scalable project.


### Tech info

  - [Kotlin](https://kotlinlang.org)
  - Multi-module project
  - MVI Architecture
  - Min SDK 21

### Libraries Used

* [Elmslie](https://github.com/vivid-money/elmslie) - architecture core library. MVI-like, inspired by ELM.
* [Dagger](https://github.com/google/dagger) for dependency injection
* [OkHttp](https://github.com/square/okhttp) and [Retrofit](https://github.com/square/retrofit) as a Http client
* [Moshi](https://github.com/square/moshi) for data parsing
* [Room](https://developer.android.com/training/data-storage/room) - data persistence library
* [Cicerone](https://github.com/terrakok/Cicerone) - lightweight navigation library which is convenient for multi-module projects
* [ViewBinding Delegate](https://github.com/androidbroadcast/ViewBindingPropertyDelegate) to make work with Android View Binding simpler
* [Coil](https://coil-kt.github.io/coil/) for image loading
* [JUnit 5](https://github.com/junit-team/junit5) - testing framework
* [Kotest](https://kotest.io/) - testing framework
* [Mockito](https://site.mockito.org/) and [Mockito Kotlin](https://github.com/mockito/mockito-kotlin) as a mocking framework for unit tests
