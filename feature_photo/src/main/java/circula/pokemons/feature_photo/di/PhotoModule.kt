package circula.pokemons.feature_photo.di

import circula.pokemons.domain_photo.feature_api.PhotoFeatureApi
import circula.pokemons.feature_photo.feature_api.PhotoFeatureApiImpl
import dagger.Binds
import dagger.Module

@Module
abstract class PhotoModule {

    @Binds
    internal abstract fun bindPhotoFeatureApi(impl: PhotoFeatureApiImpl): PhotoFeatureApi
}
