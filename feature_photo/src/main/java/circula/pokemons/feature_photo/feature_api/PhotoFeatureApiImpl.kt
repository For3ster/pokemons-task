package circula.pokemons.feature_photo.feature_api

import circula.pokemons.domain_photo.feature_api.PhotoFeatureApi
import circula.pokemons.feature_photo.PhotoFragment
import com.github.terrakok.cicerone.Screen
import com.github.terrakok.cicerone.androidx.FragmentScreen
import javax.inject.Inject

internal class PhotoFeatureApiImpl @Inject constructor() : PhotoFeatureApi {

    override fun getScreen(title: String, imageUrl: String): Screen =
        FragmentScreen { PhotoFragment.newInstance(title, imageUrl) }
}
