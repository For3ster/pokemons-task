package circula.pokemons.feature_photo

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import circula.pokemons.common_mvi.setOnBackPressedCallback
import circula.pokemons.common_navigation.router
import circula.pokemons.feature_photo.databinding.FragmentPhotoBinding
import coil.load

private const val ARG_TITLE = "ARG_TITLE"
private const val ARG_IMAGE = "ARG_IMAGE"

internal class PhotoFragment : Fragment(R.layout.fragment_photo) {

    private val binding by viewBinding(FragmentPhotoBinding::bind)
    private val title: String by lazy { arguments?.getString(ARG_TITLE)!! }
    private val imageUrl: String by lazy { arguments?.getString(ARG_IMAGE)!! }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.toolbarTitle.text = title
        binding.image.load(imageUrl)

        binding.toolbar.setNavigationOnClickListener { router.exit() }
        setOnBackPressedCallback { router.exit() }
    }

    companion object {

        fun newInstance(
            title: String,
            imageUrl: String
        ): Fragment = PhotoFragment().apply {
            arguments = bundleOf(
                ARG_TITLE to title,
                ARG_IMAGE to imageUrl
            )
        }
    }
}
