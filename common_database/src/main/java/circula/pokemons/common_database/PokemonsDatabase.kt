package circula.pokemons.common_database

import androidx.room.Database
import androidx.room.RoomDatabase
import circula.pokemons.domain_pokemons.PokemonsDao
import circula.pokemons.domain_pokemons.dto.PokemonEntity

@Database(entities = [PokemonEntity::class], version = 1)
abstract class PokemonsDatabase : RoomDatabase() {

    abstract fun pokemonsDao(): PokemonsDao
}
