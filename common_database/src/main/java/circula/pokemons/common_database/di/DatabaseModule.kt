package circula.pokemons.common_database.di

import android.content.Context
import androidx.room.Room
import circula.pokemons.domain_pokemons.PokemonsDao
import circula.pokemons.common_database.PokemonsDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(
        context: Context,
    ): PokemonsDatabase =
        Room.databaseBuilder(
            context,
            PokemonsDatabase::class.java, "pokemons-db"
        )
            .build()

    @Provides
    @Singleton
    fun provideDao(database: PokemonsDatabase): PokemonsDao =
        database.pokemonsDao()
}
