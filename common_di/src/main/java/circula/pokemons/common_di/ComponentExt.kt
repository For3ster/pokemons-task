@file:Suppress("UNCHECKED_CAST")

package circula.pokemons.common_di

import android.content.Context
import androidx.fragment.app.Fragment

inline fun <reified T> Context.getComponent(): T =
    (applicationContext as ComponentHolder<T>).getComponent(T::class.java)


inline fun <reified T> Fragment.getComponent(): T =
    (requireActivity().applicationContext as ComponentHolder<T>).getComponent(T::class.java)
