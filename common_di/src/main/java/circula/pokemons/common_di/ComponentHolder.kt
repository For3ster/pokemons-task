package circula.pokemons.common_di

interface ComponentHolder<T> {

    fun getComponent(componentClass: Class<out T>): T
}
