package circula.pokemons.domain_photo.feature_api

import com.github.terrakok.cicerone.Screen

interface PhotoFeatureApi {

    fun getScreen(
        title: String,
        imageUrl: String,
    ): Screen
}
